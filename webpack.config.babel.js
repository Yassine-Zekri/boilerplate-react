import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';

function getPlugins(mode) {
  const isProd = mode === 'production';

  // eslint-disable-next-line no-unused-vars
  let plugins = [
    new HtmlWebpackPlugin({
      template: './src/index.html',
    }),
  ];

  if (isProd) {
    plugins = plugins.concat([new CleanWebpackPlugin()]);
  }
}

export default (env, argv) => ({
  entry: './src/index.js',
  output: {
    path: path.join(__dirname, '/dist'),
    filename: 'bundle.js',
  },
  devServer: {
    contentBase: path.join(__dirname, '/dist'),
    open: true,
    port: 4000,
    hot: true,
    overlay: true,
  },
  devtool: argv.mode === 'development' ? 'source-map' : '',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            options: {
              bypassOnDebug: true,
              disable: true,
            },
          },
        ],
      },
    ],
  },
  plugins: getPlugins(argv.mode),
});
